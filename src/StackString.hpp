#ifndef StackString_h
#define StackString_h

#include<Arduino.h>

namespace Stack
{

template <int LENGTH = 32>
struct StackString
{
    StackString();
    StackString(const char* string);
    ~StackString();

    void prepend(const char* string);
    void append(const char* string);
    void append(const char character);
    void append(int number);

    bool empty();
    void clear();

    char* c_str();
    bool equals(StackString string);
    bool equals(char* string);

    bool startsWith(char *string);
    bool isDigit();
    int toInt();

    int length();
    int find(const char* needle, int nth = 1);
    StackString<LENGTH> substring(int startIndex, int length = -1);

    void print();

  private:
    char _string[LENGTH + 1] = {'\0'};
};


template <int LENGTH>
StackString<LENGTH>::StackString()
{
  _string[LENGTH] = {'\0'};
}

template <int LENGTH>
StackString<LENGTH>::StackString(const char *string)
{
  snprintf(_string, sizeof(_string), string);
  _string[LENGTH] = {'\0'};
}

template <int LENGTH>
StackString<LENGTH>::~StackString()
{
}

template <int LENGTH>
void StackString<LENGTH>::prepend(const char *front)
{
  // Save temporary string
  char temp[LENGTH + 1] = {'\0'};
  strncpy(temp, _string, sizeof(_string));

  // Set the string to prepend and append the original string (stored in temp)
  snprintf(_string, strlen(front) + 1, front);
  append(temp);
}

template <int LENGTH>
void StackString<LENGTH>::append(const char *string)
{
  if (snprintf(_string + strlen(_string), sizeof(_string) - strlen(_string), "%s", string) < 0) // Means append did not (entirely) fit
  {
    _string[LENGTH + 1] = '\0';
  }
}

template <int LENGTH>
void StackString<LENGTH>::append(const char character)
{
  if (snprintf(_string + strlen(_string), sizeof(_string) - strlen(_string), "%c", character) < 0) // Means append did not (entirely) fit
  {
    _string[LENGTH + 1] = '\0';
  }
}

template <int LENGTH>
void StackString<LENGTH>::append(int number)
{
  if (snprintf(_string + strlen(_string), sizeof(_string) - strlen(_string), "%d", number) < 0) // Means append did not (entirely) fit
  {
    _string[LENGTH + 1] = '\0';
  }
}

template <int LENGTH>
bool StackString<LENGTH>::empty()
{
  if (_string[0] == '\0')
  {
    return true;
  }

  return false;
}

template <int LENGTH>
void StackString<LENGTH>::clear()
{
  _string[0] = '\0';
}

template <int LENGTH>
char *StackString<LENGTH>::c_str()
{
  return _string;
}

template <int LENGTH>
bool StackString<LENGTH>::equals(StackString string)
{
  return equals(string.c_str());
}

template <int LENGTH>
bool StackString<LENGTH>::equals(char *string)
{
  if (strncmp(_string, string, strlen(string)) == 0)
  {
    return true;
  }

  return false;
}

template <int LENGTH>
bool StackString<LENGTH>::startsWith(char *string)
{
  int inputLength = strlen(string);

  if (inputLength > length() || inputLength < 1)
  {
    return false;
  }

  for (int i = 0; i < inputLength; i++)
  {
    if (_string[i] != string[i])
    {
      return false;
    }
  }

  return true;
}

template <int LENGTH>
bool StackString<LENGTH>::isDigit()
{
  for (int i = 0; i < length(); i++)
  {
    if (!isdigit(_string[i]))
    {
      return false;
    }
  }

  return true;
}

template <int LENGTH>
int StackString<LENGTH>::toInt()
{
  return atoi(_string);
}

template <int LENGTH>
int StackString<LENGTH>::length()
{
  return strlen(_string);
}

template <int LENGTH>
int StackString<LENGTH>::find(const char *needle, int nth)
{
    char *rest = _string;
    for (int occurrence = 1; occurrence <= nth; occurrence++)
    {
      rest = strstr(rest, needle);
      if (!rest)
        return -1;
      else if (occurrence != nth)
        rest++;
    }
    return rest - _string;
}

template <int LENGTH>
StackString<LENGTH> StackString<LENGTH>::substring(int startIndex, int length)
{
  char output[LENGTH] = {'\0'};
  if(length < 0){
    snprintf(output, LENGTH, &_string[startIndex]); // rest of string
  } else{
    snprintf(output, length + 1, &_string[startIndex]);
  }

  return StackString<LENGTH>(output);
}

template <int LENGTH>
void StackString<LENGTH>::print()
{
#ifdef DEBUG
  for (int i = 0; i <= LENGTH + 1; i++) // Shows one index after the last allocated byte (to check for overflows).
  {
    Serial.print("[");
    Serial.print(_string[i]);
    Serial.print("]");
  }
  Serial.println();
#else
  Serial.println(_string);
#endif
}
}


#endif
