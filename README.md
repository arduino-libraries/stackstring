# StackString

## Intention
This library was written to accommodate a need for a string class but without the (memory) disadvantages that regular `std::string` brings.
I needed the functionalities of std::string. However, the way `std::string` uses dynamic memory caused too many issues on my Arduino/ESP project.
That is the reason that I decided to write this class. I'm sure there's better ways to go about it but this way of doing it works just fine for me.

The reason I share this is because I hope it will help anyone of you experiencing the same issues as I did.
At the same time, the code is a far stretch from perfect or a full release, so any improvements contributed by you are very much welcome.

## Status
The project is still in an somewhat experimental status. The goal is to make it as similar to `std::string` as possible. To do so, some functionality might change.

## Features

### Basics
You can define StackStrings of any length you need. The maximum lenghth of the string needs to be known at compile time.

`StackString<100> myString = StackString<100>("Hello, World!");`

The library implements several features known in regular std::string and can always be returned as a regular character array using `myString.c_str()`.

**Note: In all usage examples below, assume `myString` has been initialized, unless specified otherwise.**

### Null-Termination
Note that the example string above can store 100 characters in any way you'd like.
The actual size (internally) is 101 characters where the 101st characters stores the null-terminator `\0`

So storing a `StackString<2>` takes up 3 bytes of storage where the internal character array looks like this: `ab\0`.
# Functions

### prepend(...)

Adds whatever is passed to the function to the front of the StackString.
  
#### Variants
`prepend(const char* string)` prepends a character array to the StackString.
`prepend(const char character)` prepends a single character to the StackString.
`prepend(int number)` prepends a numeric as text to the StackString.

#### Usage
`myString.prepend("Everybody say: ");`
If we now use `myString.c_str();` we get the following string: `Everybody say: Hello, World!`

___
### append(...)

Adds whatever is passed to the function to the end of the StackString.

#### Variants
`append(const char* string)` appends a character array to the StackString.
`append(const char character)` appends a single character to the StackString.
`append(int number)` appends a numeric as text to the StackString.

#### Usage
`myString.append(" How are you doing?");`
If we now use `myString.c_str();` we get the following string: `Hello, World! How are you doing?`
___
### bool empty()

Returns wether or not the string is empty.

#### Usage
`StackString<100> myString1 = StackString<100>("I am not empty");`
`myString1.empty();` returns `false`

`StackString<100> myString2 = StackString<100>();`
`myString2.empty();` returns `true`
___
### clear()

Empties the string.

#### Usage
`myString.clear();`
If we now use `myString.c_str();` we get an empty string. (first character is `\0`)

___
### bool equals(...)

Returns wether or not the readable string equals a provided string.

#### Variants
`equals(StackString string)`
`equals(char* string)`

#### Usage
`myString.equals("Hello, World!");` returns `true`
`myString.equals("I am different!");` returns `false`
___
### int length()

Returns the length of a string as `integer`.

#### Usage
`StackString<100> myString1 = StackString<100>("http://arjenstens.com");`
`myString1.length();` returns `21`
___
### int find(...)

Returns the index at which index the given character is present as an `integer`. Returns `-1` when no match can be found.

#### Variants
`find(const char* needle)`
`find(const char* needle, int nth)` where `nth` stands for the occurence. For example `2` for the second occurrence of the matching character.

#### Usage
`StackString<100> myString1 = StackString<100>("http://arjenstens.com");`

`myString1.find("s");` returns `12`
`myString1.find("s", 2);` returns `16`
___
### print()

Simply prints out the string using `Serial.println(...);`
___

## Contributing
As mentioned before I welcome improvents on the code to make it better for everyone.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExMDIyODE2MzYsNzEyNDgxNTA4LC0xNT
E1MjA0ODU0XX0=
-->